# Desktop TUI app prototype, on Linux, in C# / DotNet[8] / Terminal.Gui (gui.cs), using VSCode

![TerminalGuiConsole1.png](./TerminalGuiConsole1.png?raw=true "Screenshot")

## Instructions for downloading/installing .Net 'Core'

<https://dotnet.microsoft.com/download>
<https://docs.microsoft.com/en-us/dotnet/core/install/linux?WT.mc_id=dotnet-35129-website>

## Instructions/example of adding Terminal.GUI to dotnet and creating app ui in VSCode

dotnet add package Terminal.Gui --version 1.17.0

## Terminal.Gui source

<https://github.com/migueldeicaza/gui.cs>

## Terminal.Gui Overview

<https://migueldeicaza.github.io/gui.cs/articles/overview.html>

## Example of creating an app

<https://markjames.dev/2020-10-25-developing-a-cli-music-player-csharp/>

## Example of creating app

<https://curatedcsharp.com/p/gui-migueldeicaza-guics/index.html>


##  Issues

-On GhostBSD (FreeBSD), apps should build OK, but you may get an error:

```
Curses failed to initialize, the exception is: Error loading native library "libncursesw.so.6, libncursesw.so.5
```
 even after ncurses library 6.4_1 installed. If so, put 
 ```
 Application.UseSystemConsole = true;
 ```
  before 
 ```
  Application.Init();
```

## Terminal Gui Designer (Alpha)

### Install

dotnet tool install --global TerminalGuiDesigner

### UnInstall

dotnet tool uninstall --global TerminalGuiDesigner

### Update

dotnet tool update --global TerminalGuiDesigner

### Run

TerminalGuiDesigner Form1.cs

### Color Issues? Try

TerminalGuiDesigner --usc
