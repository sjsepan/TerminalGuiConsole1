using System;
using System.Threading.Tasks;
using Terminal.Gui;
//TODO:v2.0: see https://github.com/gui-cs/Terminal.Gui/discussions/2448
namespace TerminalGuiConsole1
{
    public class MainView : Window
    {
        private const string ACTION_INPROGRESS = " ...";
        private const string ACTION_DONE = " done";
        //TODO:need to simulate marquee of ProgressBar control
        private const string StatusBar_MarqueeOn = "   ***    ";
        private const string StatusBar_MarqueeOff = "          ";
        // Use this enum to access status bar items via index, as in:
        //  Application.Top.StatusBar.Items[(Int32)StatusBar_ItemIndex.Status].Title
        // or access them directly , as in:
        //  _statusMessage.Title
        private enum StatusBar_ItemIndex
        {
            Status = 0,
            Error = 1,
            Progress = 2,
            Action = 3,
            Dirty = 4
        }

        #region declare menu
        private MenuBar menu;
        #endregion declare menu

        #region declare status
        private StatusBar statusBar;
        private StatusItem _statusMessage;
        private StatusItem _errorMessage;
        private StatusItem _progressBar;
        private StatusItem _pictureAction;
        private StatusItem _pictureDirty;
        #endregion declare status

        #region declare controls
        private Label _lblSomeInt;
        private Label _lblSomeOtherInt;
        private Label _lblStillAnotherInt;
        private TextField _txtSomeInt;
        private TextField _txtSomeOtherInt;
        private TextField _txtStillAnotherInt;
        private Label _lblSomeString;
        private Label _lblSomeOtherString;
        private Label _lblStillAnotherString;
        private TextField _txtSomeString;
        private TextField _txtSomeOtherString;
        private TextField _txtStillAnotherString;
        private Label _lblSomeBoolean;
        private Label _lblSomeOtherBoolean;
        private Label _lblStillAnotherBoolean;
        private CheckBox _chkSomeBoolean;
        private CheckBox _chkSomeOtherBoolean;
        private CheckBox _chkStillAnotherBoolean;
        private Label _lblSomeDate;
        private Label _lblSomeOtherDate;
        private Label _lblStillAnotherDate;
        private DateField _dtSomeDate;
        private DateField _dtSomeOtherDate;
        private DateField _dtStillAnotherDate;
        private Button cmdRun;
        #endregion declare controls

         public MainView(string title) :
            base(title)
        {
            try
            {
                InitStyle();
                InitControls();
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(ex.Message);
            }
        }

        public void InitStyle()
        {
            // LayoutStyle = LayoutStyle.Computed;
            X = 0;
            ///Y=*1*/, // Leave one row for the toplevel menu
            //Note:Y does not have to be 1 to leave space if menu attached to current view; 1 causes extra space
            Y = 0;
            // By using Dim.Fill(), it will automatically resize without manual intervention
            Width = Dim.Fill();
            Height = Dim.Fill();
        }

        private void InitControls()
        {
            #region define menu
            menu = new MenuBar(
			[
				new MenuBarItem("_File", new MenuItem[]
                {
                    new("_New", "", FileNew_Clicked, null, null, Key.CtrlMask|Key.N),
                    new("_Open...", "",FileOpen_Clicked, null, null, Key.CtrlMask|Key.O),
                    new("_Save", "",FileSave_Clicked, null, null, Key.CtrlMask|Key.S),
                    new("Save _As...", "",FileSaveAs_Clicked, null, null, Key.Null),
                    new("---", "", null, null, null, Key.Null),
                    new("_Print", "",FilePrint_Clicked, null, null, Key.CtrlMask|Key.P),
                    new("P_rint Preview...", "",FilePrintPreview_Clicked, null, null, Key.Null),
                    new("---", "", null, null, null, Key.Null),
                    new("_Quit", "", FileQuit_Clicked, null, null, Key.Null)//Key.CtrlMask|Key.Q
                }), // end of file menu
                new MenuBarItem("_Edit", new MenuItem[]
                {
                    new("_Undo", "", EditUndo_Clicked, null, null, Key.CtrlMask|Key.Z),
                    new("_Redo", "", EditRedo_Clicked, null, null, Key.CtrlMask|Key.Y),
                    new("---", "", null, null, null, Key.Null),
                    new("Select _All", "", EditSelectAll_Clicked, null, null, Key.CtrlMask|Key.A),
                    new("Cu_t", "", EditCut_Clicked, null, null, Key.CtrlMask|Key.X),
                    new("C_opy", "", EditCopy_Clicked, null, null, Key.CtrlMask|Key.C),
                    new("_Paste", "", EditPaste_Clicked, null, null, Key.CtrlMask|Key.V),
                    new("Paste _Special...", "", EditPasteSpecial_Clicked, null, null, Key.Null),
                    new("_Delete", "", EditDelete_Clicked, null, null, Key.Delete),
                    new("---", "", null, null, null, Key.Null),
                    new("F_ind", "", EditFind_Clicked, null, null, Key.CtrlMask|Key.F),
                    new("R_eplace...", "", EditReplace_Clicked, null, null, Key.CtrlMask|Key.H),
                    new("---", "", null, null, null, Key.Null),
                    new("Re_fresh", "", EditRefresh_Clicked, null, null, Key.F5),
                    new("---", "", null, null, null, Key.Null),
                    new("Prefere_nces...", "", EditPreferences_Clicked, null, null, Key.Null)
                }), // end of edit menu
                new MenuBarItem("_Window", new MenuItem[]
                {
                    new("_New Window", "", WindowNewWindow_Clicked, null, null, Key.Null),
                    new("_Tile", "", WindowTile_Clicked, null, null, Key.Null),
                    new("_Cascade", "", WindowCascade_Clicked, null, null, Key.Null),
                    new("_Arrange All", "", WindowArrangeAll_Clicked, null, null, Key.Null),
                    new("---", "", null, null, null, Key.Null),
                    new("_Hide", "", WindowHide_Clicked, null, null, Key.Null),
                    new("_Show", "", WindowShow_Clicked, null, null, Key.Null)
                }), // end of window menu
                new MenuBarItem("_Help", new MenuItem[]
                {
                    new("_Contents", "", HelpContents_Clicked, null, null, Key.F1),
                    new("_Index", "", HelpIndex_Clicked, null, null, Key.Null),
                    new("_Online Help", "", HelpOnlineHelp_Clicked, null, null, Key.Null),
                    new("---", "", null, null, null, Key.Null),
                    new("_License Information", "", HelpLicenseInformation_Clicked, null, null, Key.Null),
                    new("Check for _Updates", "", HelpCheckForUpdates_Clicked, null, null, Key.Null),
                    new("---", "", null, null, null, Key.Null),
                    new("_About...", "", HelpAbout_Clicked, null, null, Key.Null)
                }) // end of the help menu
            ]);
            #endregion define menu

            #region define status
            statusBar = new StatusBar(
			[
				_statusMessage = new StatusItem(Key.Null, "", null),
                _errorMessage = new StatusItem(Key.Null, "", null),
                _progressBar = new StatusItem(Key.Null, StatusBar_MarqueeOff, null),
                _pictureAction = new StatusItem(Key.Null, "", null),
                _pictureDirty = new StatusItem(Key.Null, "", null)
            ]);
            //Examples
            // statusBar.Items[(Int32)StatusBar_ItemIndex.Status].Title = "NewStatusMessage";
            // or
            //_statusMessage.Title = "NewStatusMessage";
            // statusBar.Items[(Int32)StatusBar_ItemIndex.Error].Title = "NewErrorMessage";
            // or
            //_errorMessage.Title = "NewErrorMessage";
            // statusBar.Items[(Int32)StatusBar_ItemIndex.Progress].Title = "   ***    ";
            // or
            //_progressBar.Title = "   ***    ";
            // statusBar.Items[(Int32)StatusBar_ItemIndex.Action].Title = "New";
            // or
            //_pictureAction.Title = "New";
            // statusBar.Items[(Int32)StatusBar_ItemIndex.Dirty].Title = "";
            // or
            //_pictureDirty.Title = "";
            #endregion define status

            #region define controls
            _lblSomeInt = new Label("Some Integer:")
            {
                X = 0,
                Y = 0,
                Width = Dim.Percent(24)
            };

            _lblSomeOtherInt = new Label("Other Integer:")
            {
                X = Pos.Percent(25),
                Y = Pos.Top(_lblSomeInt),
                Width = Dim.Percent(24)
            };

            _lblStillAnotherInt = new Label("Another Integer:")
            {
                X = Pos.Percent(50),
                Y = Pos.Top(_lblSomeInt),
                Width = Dim.Percent(24)
            };

            _txtSomeInt = new TextField("")
            {
                X = Pos.Left(_lblSomeInt),
                Y = Pos.Top(_lblSomeInt) + 1,
                Width = Dim.Percent(24)
            };

            _txtSomeOtherInt = new TextField("")
            {
                X = Pos.Percent(25),
                Y = Pos.Top(_lblSomeOtherInt) + 1,
                Width = Dim.Percent(24)
            };

            _txtStillAnotherInt = new TextField("")
            {
                X = Pos.Percent(50),
                Y = Pos.Top(_lblStillAnotherInt) + 1,
                Width = Dim.Percent(24)
            };

            _lblSomeString = new Label("Some String:")
            {
                X = 0,
                Y = Pos.Top(_txtSomeInt) + 1,
                Width = Dim.Percent(24)
            };

            _lblSomeOtherString = new Label("Other String:")
            {
                X = Pos.Percent(25),
                Y = Pos.Top(_txtSomeInt) + 1,
                Width = Dim.Percent(24)
            };

            _lblStillAnotherString = new Label("Another String:")
            {
                X = Pos.Percent(50),
                Y = Pos.Top(_txtSomeInt) + 1,
                Width = Dim.Percent(24)
            };

            _txtSomeString = new TextField("")
            {
                X = Pos.Left(_lblSomeString),
                Y = Pos.Top(_lblSomeString) + 1,
                Width = Dim.Percent(24)
            };

            _txtSomeOtherString = new TextField("")
            {
                X = Pos.Percent(25),
                Y = Pos.Top(_lblSomeOtherString) + 1,
                Width = Dim.Percent(24)
            };

            _txtStillAnotherString = new TextField("")
            {
                X = Pos.Percent(50),
                Y = Pos.Top(_lblStillAnotherString) + 1,
                Width = Dim.Percent(24)
            };

            _lblSomeBoolean = new Label("Some Boolean:")
            {
                X = 0,
                Y = Pos.Top(_txtSomeString) + 1,
                Width = Dim.Percent(24)
            };

            _lblSomeOtherBoolean = new Label("Other Boolean:")
            {
                X = Pos.Percent(25),
                Y = Pos.Top(_lblSomeBoolean),
                Width = Dim.Percent(24)
            };

            _lblStillAnotherBoolean = new Label("Another Boolean:")
            {
                X = Pos.Percent(50),
                Y = Pos.Top(_lblSomeBoolean),
                Width = Dim.Percent(24)
            };

            _chkSomeBoolean = new CheckBox("")
            {
                X = Pos.Left(_lblSomeBoolean),
                Y = Pos.Top(_lblSomeBoolean) + 1,
                Width = Dim.Percent(24)
            };

            _chkSomeOtherBoolean = new CheckBox("")
            {
                X = Pos.Percent(25),
                Y = Pos.Top(_lblSomeOtherBoolean) + 1,
                Width = Dim.Percent(24)
            };

            _chkStillAnotherBoolean = new CheckBox("")
            {
                X = Pos.Percent(50),
                Y = Pos.Top(_lblStillAnotherBoolean) + 1,
                Width = Dim.Percent(24)
            };

            _lblSomeDate = new Label("Some Date:")
            {
                X = 0,
                Y = Pos.Top(_chkSomeBoolean) + 1,
                Width = Dim.Percent(24)
            };

            _lblSomeOtherDate = new Label("Other Date:")
            {
                X = Pos.Percent(25),
                Y = Pos.Top(_lblSomeDate),
                Width = Dim.Percent(24)
            };

            _lblStillAnotherDate = new Label("Another Date:")
            {
                X = Pos.Percent(50),
                Y = Pos.Top(_lblSomeDate),
                Width = Dim.Percent(24)
            };

            _dtSomeDate = new DateField(DateTime.Now)
            {
                X = Pos.Left(_lblSomeDate),
                Y = Pos.Top(_lblSomeDate) + 1,
                Width = Dim.Percent(24)
            };

            _dtSomeOtherDate = new DateField(DateTime.Now)
            {
                X = Pos.Percent(25),
                Y = Pos.Top(_lblSomeOtherDate) + 1,
                Width = Dim.Percent(24)
            };

            _dtStillAnotherDate = new DateField(DateTime.Now)
            {
                X = Pos.Percent(50),
                Y = Pos.Top(_lblStillAnotherDate) + 1,
                Width = Dim.Percent(24)
            };

            cmdRun = new Button("Run")
            {
                X = Pos.Percent(75),
                Y = 1
            };
            cmdRun.Clicked += CmdRun_Clicked;
            #endregion define controls

            #region add controls to top
            // Application.Top.MenuBar = menu;//any benefit?
            Application.Top.Add(menu);
            //Application.Top.StatusBar = statusBar;
            Application.Top.Add(statusBar);
            #endregion add controls to top

            #region add controls to container
            //Note:order is significant if a given control is positioned relative to another
            Add(_lblSomeInt);
            Add(_lblSomeOtherInt);
            Add(_lblStillAnotherInt);
            Add(_txtSomeInt);
            Add(_txtSomeOtherInt);
            Add(_txtStillAnotherInt);
            Add(_lblSomeString);
            Add(_lblSomeOtherString);
            Add(_lblStillAnotherString);
            Add(_txtSomeString);
            Add(_txtSomeOtherString);
            Add(_txtStillAnotherString);
            Add(_lblSomeBoolean);
            Add(_lblSomeOtherBoolean);
            Add(_lblStillAnotherBoolean);
            Add(_chkSomeBoolean);
            Add(_chkSomeOtherBoolean);
            Add(_chkStillAnotherBoolean);
            Add(_lblSomeDate);
            Add(_lblSomeOtherDate);
            Add(_lblStillAnotherDate);
            Add(_dtSomeDate);
            Add(_dtSomeOtherDate);
            Add(_dtStillAnotherDate);

            Add(cmdRun);
            #endregion add controls to container

            #region additional setup
            // login window will be appear on the center screen
            #endregion additional setup

        }

        #region Methods
        #region Handlers
        //TODO:2.0:revisit and use object sender, EventArgs args signature
        private async void FileNew_Clicked()
        {
            SetStatusMessage("New" + ACTION_INPROGRESS);
            StartProgressBar();
            StartActionIcon("New");
            await FileNewAction();
            StopActionIcon();
            StopProgressBar();
            SetStatusMessage(GetStatusMessage() + ACTION_DONE);
        }

        private void FileOpen_Clicked()
        {
            SetStatusMessage("Open" + ACTION_INPROGRESS);
            StartProgressBar();
            StartActionIcon("Open");
            FileOpenAction();
            StopActionIcon();
            StopProgressBar();
            SetStatusMessage(GetStatusMessage() + ACTION_DONE);
        }

        private void FileSave_Clicked()
        {
            SetStatusMessage("Save" + ACTION_INPROGRESS);
            StartProgressBar();
            StartActionIcon("Save");
            FileSaveAction();
            StopActionIcon();
            StopProgressBar();
            SetStatusMessage(GetStatusMessage() + ACTION_DONE);
        }

        private void FileSaveAs_Clicked()
        {
            SetStatusMessage("Save As" + ACTION_INPROGRESS);
            StartProgressBar();
            StartActionIcon("Save As");
            FileSaveAsAction();
            StopActionIcon();
            StopProgressBar();
            SetStatusMessage(GetStatusMessage() + ACTION_DONE);
        }

        private async void FilePrint_Clicked()
        {
            SetStatusMessage("Print" + ACTION_INPROGRESS);
            StartProgressBar();
            StartActionIcon("Print");
            await FilePrintAction();
            StopActionIcon();
            StopProgressBar();
            SetStatusMessage(GetStatusMessage() + ACTION_DONE);
        }

        private async void FilePrintPreview_Clicked()
        {
            SetStatusMessage("Print Preview" + ACTION_INPROGRESS);
            StartProgressBar();
            StartActionIcon("Print Preview");
            await FilePrintPreviewAction();
            StopActionIcon();
            StopProgressBar();
            SetStatusMessage(GetStatusMessage() + ACTION_DONE);
        }
        private void FileQuit_Clicked()
        {
            SetStatusMessage("Quit" + ACTION_INPROGRESS);
            StartProgressBar();
			FileQuitAction();
            StopProgressBar();
            SetStatusMessage(GetStatusMessage() + ACTION_DONE);
        }

        private async void EditUndo_Clicked()
        {
            SetStatusMessage("Undo" + ACTION_INPROGRESS);
            StartProgressBar();
            StartActionIcon("Undo");
            await EditUndoAction();
            StopActionIcon();
            StopProgressBar();
            SetStatusMessage(GetStatusMessage() + ACTION_DONE);
        }

        private async void EditRedo_Clicked()
        {
            SetStatusMessage("Redo" + ACTION_INPROGRESS);
            StartProgressBar();
            StartActionIcon("Redo");
            await EditRedoAction();
            StopActionIcon();
            StopProgressBar();
            SetStatusMessage(GetStatusMessage() + ACTION_DONE);
        }

        private async void EditSelectAll_Clicked()
        {
            SetStatusMessage("Select All" + ACTION_INPROGRESS);
            StartProgressBar();
            StartActionIcon("Select All");
            await EditSelectAllAction();
            StopActionIcon();
            StopProgressBar();
            SetStatusMessage(GetStatusMessage() + ACTION_DONE);
        }

        private async void EditCut_Clicked()
        {
            SetStatusMessage("Cut" + ACTION_INPROGRESS);
            StartProgressBar();
            StartActionIcon("Cut");
            await EditCutAction();
            StopActionIcon();
            StopProgressBar();
            SetStatusMessage(GetStatusMessage() + ACTION_DONE);
        }

        private async void EditCopy_Clicked()
        {
            SetStatusMessage("Copy" + ACTION_INPROGRESS);
            StartProgressBar();
            StartActionIcon("Copy");
            await EditCopyAction();
            StopActionIcon();
            StopProgressBar();
            SetStatusMessage(GetStatusMessage() + ACTION_DONE);
        }

        private async void EditPaste_Clicked()
        {
            SetStatusMessage("Paste" + ACTION_INPROGRESS);
            StartProgressBar();
            StartActionIcon("Paste");
            await EditPasteAction();
            StopActionIcon();
            StopProgressBar();
            SetStatusMessage(GetStatusMessage() + ACTION_DONE);
        }

        private async void EditPasteSpecial_Clicked()
        {
            SetStatusMessage("Paste Special" + ACTION_INPROGRESS);
            StartProgressBar();
            await EditPasteSpecialAction();
            StopProgressBar();
            SetStatusMessage(GetStatusMessage() + ACTION_DONE);
        }
        private async void EditDelete_Clicked()
        {
            SetStatusMessage("Delete" + ACTION_INPROGRESS);
            StartProgressBar();
            await EditDeleteAction();
            StopProgressBar();
            SetStatusMessage(GetStatusMessage() + ACTION_DONE);
        }

        private async void EditFind_Clicked()
        {
            SetStatusMessage("Find" + ACTION_INPROGRESS);
            StartProgressBar();
            StartActionIcon("Find");
            await EditFindAction();
            StopActionIcon();
            StopProgressBar();
            SetStatusMessage(GetStatusMessage() + ACTION_DONE);
        }

        private async void EditReplace_Clicked()
        {
            SetStatusMessage("Replace" + ACTION_INPROGRESS);
            StartProgressBar();
            StartActionIcon("Replace");
            await EditReplaceAction();
            StopActionIcon();
            StopProgressBar();
            SetStatusMessage(GetStatusMessage() + ACTION_DONE);
        }

        private async void EditRefresh_Clicked()
        {
            SetStatusMessage("Refresh" + ACTION_INPROGRESS);
            StartProgressBar();
            StartActionIcon("Refresh");
            await EditRefreshAction();
            StopActionIcon();
            StopProgressBar();
            SetStatusMessage(GetStatusMessage() + ACTION_DONE);
        }

        private async void EditPreferences_Clicked()
        {
            SetStatusMessage("Preferences" + ACTION_INPROGRESS);
            StartProgressBar();
            StartActionIcon("Preferences");
            await EditPreferencesAction();
            StopActionIcon();
            StopProgressBar();
            SetStatusMessage(GetStatusMessage() + ACTION_DONE);
        }
        private async void WindowNewWindow_Clicked()
        {
            SetStatusMessage("New Window" + ACTION_INPROGRESS);
            StartProgressBar();
            await WindowNewWindowAction();
            StopActionIcon();
            StopProgressBar();
            SetStatusMessage(GetStatusMessage() + ACTION_DONE);
        }
        private async void WindowTile_Clicked()
        {
            SetStatusMessage("Tile" + ACTION_INPROGRESS);
            StartProgressBar();
            await WindowTileAction();
            StopActionIcon();
            StopProgressBar();
            SetStatusMessage(GetStatusMessage() + ACTION_DONE);
        }
        private async void WindowCascade_Clicked()
        {
            SetStatusMessage("Cascade" + ACTION_INPROGRESS);
            StartProgressBar();
            await WindowCascadeAction();
            StopActionIcon();
            StopProgressBar();
            SetStatusMessage(GetStatusMessage() + ACTION_DONE);
        }
        private async void WindowArrangeAll_Clicked()
        {
            SetStatusMessage("Arrange All" + ACTION_INPROGRESS);
            StartProgressBar();
            await WindowArrangeAllAction();
            StopActionIcon();
            StopProgressBar();
            SetStatusMessage(GetStatusMessage() + ACTION_DONE);
        }
        private async void WindowHide_Clicked()
        {
            SetStatusMessage("Hide" + ACTION_INPROGRESS);
            StartProgressBar();
            await WindowHideAction();
            StopActionIcon();
            StopProgressBar();
            SetStatusMessage(GetStatusMessage() + ACTION_DONE);
        }
        private async void WindowShow_Clicked()
        {
            SetStatusMessage("Show" + ACTION_INPROGRESS);
            StartProgressBar();
            await WindowShowAction();
            StopActionIcon();
            StopProgressBar();
            SetStatusMessage(GetStatusMessage() + ACTION_DONE);
        }

        private async void HelpContents_Clicked()
        {
            SetStatusMessage("Contents" + ACTION_INPROGRESS);
            StartProgressBar();
            StartActionIcon("Help");
            await HelpContentsAction();
            StopActionIcon();
            StopProgressBar();
            SetStatusMessage(GetStatusMessage() + ACTION_DONE);
        }
        private async void HelpIndex_Clicked()
        {
            SetStatusMessage("Index" + ACTION_INPROGRESS);
            StartProgressBar();
            StartActionIcon("Index");
            await HelpIndexAction();
            StopActionIcon();
            StopProgressBar();
            SetStatusMessage(GetStatusMessage() + ACTION_DONE);
        }
        private async void HelpOnlineHelp_Clicked()
        {
            SetStatusMessage("Online Help" + ACTION_INPROGRESS);
            StartProgressBar();
            StartActionIcon("Online Help");
            await HelpOnlineHelpAction();
            StopActionIcon();
            StopProgressBar();
            SetStatusMessage(GetStatusMessage() + ACTION_DONE);
        }
        private async void HelpLicenseInformation_Clicked()
        {
            SetStatusMessage("License Information" + ACTION_INPROGRESS);
            StartProgressBar();
            StartActionIcon("License Information");
            await HelpLicenseInformationAction();
            StopActionIcon();
            StopProgressBar();
            SetStatusMessage(GetStatusMessage() + ACTION_DONE);
        }
        private async void HelpCheckForUpdates_Clicked()
        {
            SetStatusMessage("Check For Updates" + ACTION_INPROGRESS);
            StartProgressBar();
            await HelpCheckForUpdatesAction();
            StopProgressBar();
            SetStatusMessage(GetStatusMessage() + ACTION_DONE);
        }
        private void HelpAbout_Clicked()
        {
            SetStatusMessage("About" + ACTION_INPROGRESS);
            StartProgressBar();
            StartActionIcon("About");
			HelpAboutAction();
            StopActionIcon();
            StopProgressBar();
            SetStatusMessage(GetStatusMessage() + ACTION_DONE);
        }
        private void CmdRun_Clicked()
        {
            SetStatusMessage("Run" + ACTION_INPROGRESS);
            StartProgressBar();
            StartActionIcon("Run");
            RunAction();
            StopActionIcon();
            StopProgressBar();
            SetStatusMessage(GetStatusMessage() + ACTION_DONE);
        }
        #endregion Handlers

        #region Actions
        private static async Task DoSomething()
        {
            for (int i = 0; i < 3; i++)
            {
                //_progressBar.Pulse();
                Application.DoEvents();
                await Task.Delay(1000);
            }
        }

        private void RunAction()
        {
            _chkStillAnotherBoolean.Checked = _chkSomeOtherBoolean.Checked;
            _chkSomeOtherBoolean.Checked = _chkSomeBoolean.Checked;
            _chkSomeBoolean.Checked = !_chkSomeBoolean.Checked;
        }
        private static async Task FileNewAction()
        {
            await DoSomething();
        }
        private void FileOpenAction()
        {
            try
            {
                OpenDialog openDialog = new("Open...", "Open a file")
                {
                    //AllowedFileTypes = new String[] {},
                    AllowsOtherFileTypes = true,
                    AllowsMultipleSelection = true
                };
                Application.Run (openDialog);

                if (!openDialog.Canceled)
                {//TODO:v2.0:revisit and remove nstack ustring
                    SetStatusMessage(GetStatusMessage() + openDialog.FilePath.ToString());//Ignore "Remove redundant 'ToString' call (RCS1097)", or will cause "Argument 1: cannot convert from 'NStack.ustring' to 'string'(CS1503)"
                }
                else
                {
                    SetStatusMessage(GetStatusMessage() + " cancelled ");
                }
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine("Exception: {0}", ex.Message);
            }
        }
        private void  FileSaveAction(bool isSaveAs =false)
        {
            try
            {
                SaveDialog saveDialog = new(isSaveAs ? "Save As..." : "Save...", "Save a file")
                {
                    //AllowedFileTypes = new String[] {},
                    AllowsOtherFileTypes = true,
                };
                Application.Run (saveDialog);

                if (!saveDialog.Canceled)
                {//TODO:v2.0:revisit and remove nstack ustring
                    SetStatusMessage(GetStatusMessage() + saveDialog.FilePath.ToString());//Ignore "Remove redundant 'ToString' call (RCS1097)", or will cause "Argument 1: cannot convert from 'NStack.ustring' to 'string'(CS1503)"
                }
                else
                {
                    SetStatusMessage(GetStatusMessage() + " cancelled ");
                }
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine("Exception: {0}", ex.Message);
            }
        }
        private void  FileSaveAsAction()
        {
            FileSaveAction(true);
        }
        private static async Task  FilePrintAction()
        {
            await DoSomething();
            //FilePrintAction(false);
        }
        private static async Task  FilePrintPreviewAction()
        {
            await DoSomething();
            //FilePrintPreviewAction(true);
        }
        private static void FileQuitAction()
        {
            Application.RequestStop();
        }
        private static async Task EditUndoAction()
        {
            await DoSomething();
        }
        private static async Task EditRedoAction()
        {
            await DoSomething();
        }
        private static async Task EditSelectAllAction()
        {
            await DoSomething();
        }
        private static async Task EditCutAction()
        {
            await DoSomething();
        }
        private static async Task EditCopyAction()
        {
            await DoSomething();
        }
        private static async Task EditPasteAction()
        {
            await DoSomething();
        }
        private static async Task EditPasteSpecialAction()
        {
            await DoSomething();
        }
        private static async Task EditDeleteAction()
        {
            await DoSomething();
        }
        private static async Task EditFindAction()
        {
            await DoSomething();
        }
        private static async Task EditReplaceAction()
        {
            await DoSomething();
        }
        private static async Task EditRefreshAction()
        {
            await DoSomething();
        }
        private static async Task EditPreferencesAction()
        {
            await DoSomething();
        }
        private static async Task WindowNewWindowAction()
        {
            await DoSomething();
        }
        private static async Task WindowTileAction()
        {
            await DoSomething();
        }
        private static async Task WindowCascadeAction()
        {
            await DoSomething();
        }
        private static async Task WindowArrangeAllAction()
        {
            await DoSomething();
        }
        private static async Task WindowHideAction()
        {
            await DoSomething();
        }
        private static async Task WindowShowAction()
        {
            await DoSomething();
        }
        private static async Task HelpContentsAction()
        {
            await DoSomething();
        }
        private static async Task HelpIndexAction()
        {
            await DoSomething();
        }
        private static async Task HelpOnlineHelpAction()
        {
            await DoSomething();
        }
        private static async Task HelpLicenseInformationAction()
        {
            await DoSomething();
        }
        private static async Task HelpCheckForUpdatesAction()
        {
            await DoSomething();
        }

        private static void HelpAboutAction()
        {
            MessageBox.Query(30, 10, "About", "Terminal.Gui Console1 by Stephen J Sepan\nUsing Terminal.Gui (gui.cs) by Miguel DeIcaza\nBased on demo by Ali Bahraminezhad\nVersion 0.3", "Ok");
        }
        #endregion Actions

        #region Utility

        private string GetStatusMessage()
        {
            return _statusMessage.Title.ToString();
            // return Application.Top.StatusBar.Items[(Int32)StatusBar_ItemIndex.Status].Title.ToString();
        }

        private void SetStatusMessage(string statusMessage)
        {
            _statusMessage.Title = statusMessage;
            // Application.Top.StatusBar.Items[(Int32)StatusBar_ItemIndex.Status].Title = statusMessage;
            SetNeedsDisplay();
        }

        private void SetErrorMessage(string errorMessage)
        {
            _errorMessage.Title = errorMessage;
            // Application.Top.StatusBar.Items[(Int32)StatusBar_ItemIndex.Error].Title = errorMessage;
            SetNeedsDisplay();
        }

        private void StartProgressBar()
        {
            // _progressBar.Fraction = 0.33;
            // _progressBar.PulseStep=0.1;
            // _progressBar.Visible = true;
            // _progressBar.Pulse();
            _progressBar.Title = StatusBar_MarqueeOn;
            // Application.Top.StatusBar.Items[(Int32)StatusBar_ItemIndex.Progress].Title = StatusBar_MarqueeOn;
            SetNeedsDisplay();
        }

        private void StopProgressBar()
        {
            // _progressBar.Pulse();
            //Application.DoEvents();
            // _progressBar.Visible = false;
            // _progressBar.Fraction = 0.0;
            // _progressBar.PulseStep=0.0;
            _progressBar.Title = StatusBar_MarqueeOff;
            // Application.Top.StatusBar.Items[(Int32)StatusBar_ItemIndex.Progress].Title = StatusBar_MarqueeOff;
            SetNeedsDisplay();
        }

        private void StartActionIcon(string actionName)
        {
            _pictureAction.Title = actionName;
            // Application.Top.StatusBar.Items[(Int32)StatusBar_ItemIndex.Action].Title = actionName;
            SetNeedsDisplay();
        }

        private void StopActionIcon()
        {
            _pictureAction.Title = "";
            // Application.Top.StatusBar.Items[(Int32)StatusBar_ItemIndex.Action].Title = "";
            SetNeedsDisplay();
        }

        private void SetDirtyIcon(bool isSet)
        {
            _pictureDirty.Title = isSet ? "Dirty" : "";
            // Application.Top.StatusBar.Items[(Int32)StatusBar_ItemIndex.Dirty].Title = (isSet ? "Dirty" : "");
            SetNeedsDisplay();
        }

        //TODO:adapt this sample  to my statusbar
        // var progress = new ProgressBar (new Rect (68, 1, 10, 1));
        // bool timer (MainLoop caller)
        // {//TODO:use DoEvents, to make more responsive than original sample?
        // 	progress.Pulse ();
        // 	return true;
        // }
        // statusBarFrameView.Add (new Label ("label in frameview") {
        //     X = 0,Y = 0
        // });
        // Application.MainLoop.AddTimeout (TimeSpan.FromMilliseconds (300), timer);
        #endregion Utility
        #endregion Methods
    }
}