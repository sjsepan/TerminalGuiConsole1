﻿using System;
using Terminal.Gui;

// uses Terminal.Gui (gui.cs) by Miguel DeIcaza on https://github.com/migueldeicaza/gui.cs
// based on sample by Ali Bahraminezhad on https://itnext.io/terminal-console-user-interface-in-net-core-4e978f1225b
namespace TerminalGuiConsole1
{
    static class Program
    {
        // static void Main(string[] args)
        static int Main(string[] args)
        {
            //default to fail code
            int returnValue = -1;

            try
            {
				Application.UseSystemConsole = true;
				Application.Init();
                // Application.Top.LayoutStyle = LayoutStyle.Computed;

                //MainView (derived from Window) is also a TopLevel
                MainView mainWindow = new("Terminal.Gui Console1");

                Application.Top.Add(mainWindow);
                Application.Run(Application.Top);

                //Note:required, or terminal may no longer behave correctly after Ctrl-Q
                Application.Shutdown();

                //return success code
                returnValue = 0;
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(ex.Message);
            }

            return returnValue;
        }
    }
}
